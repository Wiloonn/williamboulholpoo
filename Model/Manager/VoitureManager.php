<?php

class VoitureManager extends DbManager
{
    public function __construct()
    {
        parent::__construct();
    }

    public function selectAll()
    {
        $voiture = [];
        $sql = 'SELECT * FROM voiture';
        foreach ($this->bdd->query($sql) as $row) {
            $voiture[] = new Voiture($row['id'], $row['marque'], $row['energie'], $row['boite_auto']);
        }
        return $voiture;
    }

    public function insert(Voiture $voiture)
    {
        $marque = $voiture->getMarque();
        $energie = $voiture->getEnergie();
        $boite_auto = $voiture->getBoiteAuto();
        $requete = $this->bdd->prepare("INSERT INTO voiture (marque, energie, boite_auto) VALUES (?,?,?)");
        $requete->bindParam(1, $marque);
        $requete->bindParam(2, $energie);
        $requete->bindParam(3, $boite_auto);
        $requete->execute();
        $voiture->setId($this->bdd->lastInsertId());
    }

    public function delete($id)
    {
        $requete = $this->bdd->prepare("DELETE FROM voiture where id = ?");
        $requete->bindParam(1, $id);
        $requete->execute();
    }

    public function select($id)
    {
        $requete = $this->bdd->prepare("SELECT * FROM voiture WHERE id=?");
        $requete->bindParam(1, $id);
        $requete->execute();
        $res = $requete->fetch();
        $voiture = new voiture($res['id'], $res['marque'], $res['energie'], $res['boite_auto']);

        return $voiture;
    }
}
?>
