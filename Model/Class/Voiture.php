<?php

class Voiture
{
    private $id;
    private $marque;
    private $energie;
    private $boite_auto;

    /**
     * Voiture constructor.
     * @param $id
     * @param $marque
     * @param $energie
     * @param $boite_auto
     */
    public function __construct($id = null, $marque, $energie, $boite_auto)
    {
        $this->id = $id;
        $this->marque = $marque;
        $this->energie = $energie;
        $this->boite_auto = $boite_auto;
    }

    /**
     * @return mixed|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed|null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * @param mixed $marque
     */
    public function setMarque($marque)
    {
        $this->marque = $marque;
    }

    /**
     * @return mixed
     */
    public function getEnergie()
    {
        return $this->energie;
    }

    /**
     * @param mixed $energie
     */
    public function setEnergie($energie)
    {
        $this->energie = $energie;
    }

    /**
     * @return mixed
     */
    public function getBoiteAuto()
    {
        return $this->boite_auto;
    }

    /**
     * @param mixed $boite_auto
     */
    public function setBoiteAuto($boite_auto)
    {
        $this->boite_auto = $boite_auto;
    }
}