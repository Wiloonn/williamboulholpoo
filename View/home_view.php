<html>
<head>
    <?php
    require 'Parts/stylesheets.html';
    ?>
</head>

<body>
<header>
    <!-- As a heading -->
    <nav class="navbar navbar-light bg-light">
        <span class="navbar-brand mb-0 h1">Les voitures !</span>
    </nav>
</header>
<br>

<a href="../williamBoulhol/index.php?controller=voiture&action=addForm">
    <button class="btn btn-primary">Ajouter une voiture</button>
</a>
<br>
<br>
<?php
foreach ($voiture as $voit) {

?>
<div class="card" style="width: 18rem;">
    <h2><?php echo $voit->getMarque()?></h2>
    <div class="card-body">
        <p class="card-text">Énergie : <?php echo $voit->getEnergie()?></p>
        <p class="card-text">Boite Auto : <?php if($voit->getBoiteAuto() == 1){
            echo('Oui');
            } else {
            echo('Non');
            }
            ?></p>
        <a href="../williamBoulhol/index.php?controller=voiture&action=displayOne&id=<?php echo $voit->getId()?>" class="btn btn-primary">Voir plus</a>
        <a href="../williamBoulhol/index.php?controller=voiture&action=delete&id=<?php echo $voit->getId()?>" class="btn btn-primary">Supprimer</a>
    </div>
</div>
<?php
}
?>

</body>
</html>