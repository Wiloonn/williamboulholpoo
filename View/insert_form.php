<html>
<head>
    <?php
    include 'Parts/stylesheets.html'
    ?>
</head>

<body>
<div class="container">

    <a href="../williamBoulhol/index.php?controller=default&action=home">
        <button style="margin-bottom:10px;" class="btn btn-primary">Revenir en arrière</button>
    </a>

    <form method="post" action="index.php?controller=voiture&action=addVoiture">
        <label>Marque</label>
        <input name="marque" class="form-control">
        <br>

        <!-- ENERGIE -->
        <label for="energie">Energie :</label><br>

        <select name="energie" id="energie">
            <option value="">Choisissez une option</option>
            <option value="essence">Essence</option>
            <option value="diesel">Diesel</option>
            <option value="hybride">Hybride</option>
            <option value="electrique">Electrique</option>
        </select><br>

        <!-- BOITE AUTO -->
        <br>
        <label for="boite_auto">Boite Auto :</label><br>

        <select name="boite_auto" id="boite_auto">
            <option value="">Choisissez une option</option>
            <option value="1">Oui</option>
            <option value="0">Non</option>
        </select><br>
        <br>
        <input class="btn btn-primary" type="submit" value="valider">
    </form>
</div>
<?php
include 'Parts/scripts.html'
?>
</body>