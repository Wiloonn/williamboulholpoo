<?php

class VoitureController {
    private $voituremanager;

    public function __construct()
    {
        $this->voituremanager = new VoitureManager();
    }

    public function addForm()
    {
        require 'View/insert_form.php';
    }

    public function persistForm()
    {
        $voiture = new Voiture(null, $_POST['marque'], $_POST['energie'], $_POST['boite_auto']);
        $voituremanager = new VoitureManager();
        $voituremanager->insert($voiture);
        header('Location: /williamBoulhol/index.php?controller=default&action=home');
    }

    public function delete($id)
    {
        $voituremanager = new VoitureManager();
        $voituremanager->delete($id);
        header('Location: /williamBoulhol/index.php?controller=default&action=home');
    }

    public function displayOne($id){
        $voiture = $this->voituremanager->select($id);
        require 'View/detail.php';
    }
}
